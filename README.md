# Self-Supervised-GAN-Compression-Appendix

In this repo, we develop a self-supervised GAN compression technique that can be easily applied to new tasks and models, and enable meaningful comparisons between different compression granularities.